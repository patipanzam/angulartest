import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [{
  path: '',
  redirectTo: 'homepage',
  pathMatch: 'full'
}, {
  path: 'homepage',
  loadChildren: () => import('./modules/homepage/homepage.module').then(m => m.HomepageModule)
}, {
  path: '**',
  redirectTo: ''
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

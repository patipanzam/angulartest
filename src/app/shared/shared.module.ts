import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgZorroAntdModule, NZ_I18N, th_TH ,NzToolTipModule} from 'ng-zorro-antd';
import { ModalModule } from 'ngx-bootstrap';
import { NzTableModule, NzTabsModule, NzTreeModule, NzDropDownModule, NzCheckboxModule, NzDividerModule, NzModalModule } from 'ng-zorro-antd';

import { NumberOnlyField } from './number-only-field.directive';

const _IMPORT_OTHERS = [
  /* import other lib at here */
  ModalModule.forRoot(),
  NgZorroAntdModule,
  NzTableModule,
  NzTabsModule,
  NzTreeModule,
  NzDropDownModule,
  NzCheckboxModule,
  NzDividerModule,
  NzModalModule,

];

const _EXPORTS = [
  /* export at here */
  // Module
  NzTableModule,
  NzTabsModule,
  NzTreeModule,
  NzDropDownModule,
  NzCheckboxModule,
  NzDividerModule,
  NzModalModule,
  ModalModule,

  // Component


  // Pipe

  // Directive
  NumberOnlyField

];

const _DECLARATIONS = [
  /* declaration shared component at here */
  // Component

  // Pipe

  // Directive
  NumberOnlyField
];

const _PROVIDERS = [
  /* provider service at here */
 { provide: NZ_I18N, useValue: th_TH },
];

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    _IMPORT_OTHERS
  ],
  exports: [
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    _EXPORTS
  ],
  declarations: [_DECLARATIONS],
  providers: [_PROVIDERS]
})
export class SharedModule { }

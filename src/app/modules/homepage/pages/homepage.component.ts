import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ModalDirective } from 'ngx-bootstrap-th';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})
export class HomepageComponent implements OnInit {

  @ViewChild('alertModal') alertModal: ModalDirective;

  public inputForm: FormGroup;
  public checkAllRow: Boolean = false;
  public bmi: number ;
  public msgMessage = '';
  dataTable: any[] = [];

    /* Table config */
    keyword = '';
    allChecked = false;
    indeterminate = false;
    pageIndex = 1;
    pageSize = 10;
    total = 0;
    dataSet = [];
    loading = true;
    sortValue = '';
    sortKey = '';

  constructor(private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    this.initForm();
    this.loadData();
  }

  initForm() {
    this.inputForm = this.formBuilder.group({
      seq: [0, []],
      name: [null, [Validators.required]],
      weight: [null, [Validators.required]],
      height: [null, [Validators.required]],
      bmi: [null, []],
    });
  }

  loadData(reset: boolean = false) {
    if (reset) {
      this.pageIndex = 1;
    }
    try {
      this.loading = false;
      const result   = this.dataTable;
      this.dataTable = result.slice((this.pageIndex - 1) * this.pageSize, this.pageSize * this.pageIndex);
      this.total   =  this.dataTable.length;
    } catch (error) { }

  }

  checkAll() {
    this.dataTable = this.dataTable.map((row, index) => {
      let newRow = {
        ...row,
        checked: !this.checkAllRow
      }
      return newRow;
    });
    this.checkAllRow = !this.checkAllRow;
    this.refreshStatus();
  }

  attachListCheck: any[] = [];
  refreshStatus(): void {
    const allChecked = this.dataTable.every((value: any) => value.checked === true);
    this.attachListCheck = this.dataTable.filter((value: any) => value.checked === true);
    const allUnChecked = this.dataTable.every((value: any) => !value.checked);
    this.allChecked = allChecked;
    this.indeterminate = (!allChecked) && (!allUnChecked);
  }

  sort(sort: { key: string, value: string }): void {
    this.sortKey = sort.key;
    this.sortValue = sort.value;
    this.loadData();
  }

  onClickEdit(data: any) {
    this.bmi = this.roundUp(data.weight / Math.pow(data.height, 2) * 10000);
    this.inputForm.setValue(data);
   }

  onDelete(data: any) {
    let index = this.dataTable.findIndex(obj => obj.seq === data);
    this.dataTable.splice(index, 1);
    this.dataTable = this.dataTable.map((object, index) => {
      return {
        ...object,
        seq: index + 1
      }
    });
    this.loadData();
  }

  onSubmitData() {
    const data = this.inputForm.getRawValue();
    let result = this.roundUp(data.weight / Math.pow(data.height, 2) * 10000);
    let massage = '';
    if (result < 18.5) {
      massage = 'you are : Underweight';
    } else if (result >= 18.5 && result <= 25) {
      massage = 'you are : Normal';
    } else if (result >= 25 && result <= 30) {
      massage = 'you are : Obese';
    } else if (result > 30) {
      massage = 'you are : Overweight';
    }
    this.openModal(massage, () => {
      this.saveData();
    });
  }

  saveData() {
    const dataTable = this.inputForm.getRawValue();
    if (dataTable.seq === 0) {
      dataTable.seq = (this.dataTable.length > 0) ? this.dataTable.length : 1;
      dataTable.bmi = this.roundUp(dataTable.weight / Math.pow(dataTable.height, 2) * 10000);
      this.dataTable.push(dataTable);
      this.dataTable.sort((a, b) => a.bmi - b.bmi);
    } else {
      let index = this.dataTable.findIndex(data => data.seq === dataTable.seq);
      dataTable.bmi = this.roundUp(dataTable.weight / Math.pow(dataTable.height, 2) * 10000);
      this.dataTable[index] = dataTable;
      this.dataTable.sort((a, b) => a.bmi - b.bmi);

    }
    this.bmi = null;
    this.inputForm.reset();
    this.initForm();
  }

  checkBmi(data) {
    let color = '';
    if (data < 18.5) {
      color = 'red';
    } else if (data >= 18.5 && data <= 25) {
      color = 'green';
    } else if (data >= 25 && data <= 30) {
      color = '#06EFF3';
    } else if (data > 30) {
      color = 'blue';
    }
      return color;
  }

  calculateBMI() {
    const data = this.inputForm.getRawValue();
    if (data.weight && data.height) {
      this.bmi = this.roundUp(data.weight / Math.pow(data.height, 2) * 10000);
    }
  }

  public callbackModal = () => { };
  openModal(msgMessage: string, fnCallback = () => { }) {
    this.msgMessage = msgMessage;
    this.alertModal.show();
    this.callbackModal = fnCallback;
  }

  private roundUp(value: any): number {
    return Math.round(Number(value) * 100) / 100;
  }

//   import com.google.gson.JsonObject;

// import java.io.BufferedWriter;
// import java.io.FileWriter;
// import java.io.IOException;
// import java.util.ArrayList;
// import java.util.HashMap;
// import java.util.List;
// import java.util.Map;

// public class GenerateFormattedTextFileFromDatabase {
//     public static void main(String[] args) {
//         List<LnLoanAdjustTransaction> listData = SqlLnTransactionDao.selLoanTrxnAll(trxn_date, false); // Retrieve data from the database

//         Map<String, List<LnLoanAdjustTransaction>> loanClassMap = new HashMap<>();

//         for (LnLoanAdjustTransaction loan : listData) {
//             String loanClass = loan.getLoanClass();

//             if (!loanClassMap.containsKey(loanClass)) {
//                 loanClassMap.put(loanClass, new ArrayList<>());
//             }

//             loanClassMap.get(loanClass).add(loan);
//         }

//         try (BufferedWriter writer = new BufferedWriter(new FileWriter("output.txt"))) {
//             StringBuilder contentBuilder = new StringBuilder();

//             for (String loanClass : loanClassMap.keySet()) {
//                 contentBuilder.append("Manual ").append(loanClass).append("(loan_class)").append("\n");
//                 List<LnLoanAdjustTransaction> loans = loanClassMap.get(loanClass);
//                 double sumPrincipal = 0;
//                 double sumTrxnAmount = 0;

//                 for (int i = 0; i < loans.size(); i++) {
//                     LnLoanAdjustTransaction loan = loans.get(i);
//                     contentBuilder.append((i + 1)).append("\t").append(loan.getPackAcctNo()).append("\t")
//                             .append(loan.getLoanNo()).append("\t").append(loan.getPrincipal()).append("\t")
//                             .append(loan.getTrxnAmount()).append("\n");

//                     double principal = Double.parseDouble(loan.getPrincipal());
//                     double trxnAmount = Double.parseDouble(loan.getTrxnAmount());
//                     sumPrincipal += principal;
//                     sumTrxnAmount += trxnAmount;
//                 }

//                 contentBuilder.append("\tSum Group Loan\t").append(sumPrincipal).append("\t").append(sumTrxnAmount).append("\n\n");
//             }

//             double totalPrincipal = loanClassMap.values()
//                     .stream()
//                     .flatMap(List::stream)
//                     .mapToDouble(loan -> Double.parseDouble(loan.getPrincipal()))
//                     .sum();

//             double totalTrxnAmount = loanClassMap.values()
//                     .stream()
//                     .flatMap(List::stream)
//                     .mapToDouble(loan -> Double.parseDouble(loan.getTrxnAmount()))
//                     .sum();

//             contentBuilder.append("\tTotal\t").append(totalPrincipal).append("\t").append(totalTrxnAmount);

//             // Write the content to the file using the StringBuilder
//             writer.write(contentBuilder.toString());
//         } catch (IOException e) {
//             e.printStackTrace();
//         }
//     }

//     // Define your LnLoanAdjustTransaction class and SqlLnTransactionDao class as needed
//     static class LnLoanAdjustTransaction {
//         // Define your fields, getters, and setters here
//     }

//     static class SqlLnTransactionDao {
//         // Define your database access methods here
//     }
// }

}

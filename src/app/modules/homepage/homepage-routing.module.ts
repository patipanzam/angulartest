import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomepageComponent } from '../homepage/pages/homepage.component';

const routes: Routes = [{
  path: '',
  data: { title: 'Homepage' },
  component: HomepageComponent,
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomepageRoutingModule { }
